from win32com.client import GetObject
import ctypes
import time
import winsound

WMI = GetObject('winmgmts:')
charging_warning = 0
low_charging = 0
battery_percentage = 0
battery_status = 0

while True:
    for battery in WMI.InstancesOf('Win32_Battery'):
        battery_percentage = battery.EstimatedChargeRemaining
        battery_status = battery.BatteryStatus

    # If battery percentage is above 95 and is still plugged in display warning.
    # Shows a persistent message box only once for every 3 consecutive times
    if battery_percentage >= 95 and (battery_status == 3 or battery_status == 6 or battery_status == 7 or battery_status == 2):
        if charging_warning == 0:
            winsound.Beep(2000,1500)
            ctypes.windll.user32.MessageBoxA(0, "Battery percentage is " + str(battery_percentage) +
                                             " and plugged in.\nUnplug the charger.", "Unplug Charger", 0x00001000L)
        charging_warning = (charging_warning + 1) % 3

    # If battery percentage is below 15 and is not plugged in display warning.
    # Shows a persistent message box only once for every 3 consecutive times
    if battery_percentage <= 15 and (battery_status == 5 or battery_status == 4 or battery_status == 11 or battery_status == 1):
        if low_charging == 0:
            winsound.Beep(2000,1500)
            ctypes.windll.user32.MessageBoxA(0, "Battery percentage is " + str(battery_percentage) +
                                             " and not plugged in.\nPlug in the charger.", "Plug Charger", 0x00001000L)
        low_charging = (low_charging + 1) % 3

    # If battery status is draining reset charging_warning to 0
    if battery_status == 5 or battery_status == 4 or battery_status == 11 or battery_status == 1:
            charging_warning = 0

    # If battery status is charging, reset low_charging to 0
    if battery_status == 8 or battery_status == 9 or battery_status == 3 or battery_status == 6 or battery_status == 7 or battery_status == 2:
            low_charging = 0

    time.sleep(60)
